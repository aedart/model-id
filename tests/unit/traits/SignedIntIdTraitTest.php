<?php

/**
 * @coversDefaultClass Aedart\Model\Id\Traits\SignedIntegerIdTrait
 */
class SignedIntIdTraitTest extends \Codeception\TestCase\Test
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    /******************************************************************************
     * Providers
     *****************************************************************************/

    /**
     * Get mock for given trait
     * @return PHPUnit_Framework_MockObject_MockObject
     */
    protected function getTraitMock(){
        $m = $this->getMockForTrait('Aedart\Model\Id\Traits\SignedIntegerIdTrait');
        return $m;
    }

    /******************************************************************************
     * Tests
     *****************************************************************************/

    /**
     * @test
     * @covers ::isIdValid
     */
    public function isIdValid(){
        $trait = $this->getTraitMock();
        $this->assertTrue($trait->isIdValid(42));
    }

    /**
     * @test
     * @covers ::isIdValid
     */
    public function negativeRange(){
        $trait = $this->getTraitMock();
        $this->assertTrue($trait->isIdValid(-PHP_INT_MAX));
    }

    /**
     * @test
     * @covers ::isIdValid
     */
    public function positiveRange(){
        $trait = $this->getTraitMock();
        $this->assertTrue($trait->isIdValid(PHP_INT_MAX));
    }
}