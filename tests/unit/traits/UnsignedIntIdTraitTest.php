<?php

use Aedart\Model\Id\Traits\UnsignedIntegerIdTrait;

/**
 * @coversDefaultClass Aedart\Model\Id\Traits\UnsignedIntegerIdTrait
 */
class UnsignedIntIdTraitTest extends \Codeception\TestCase\Test
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    /******************************************************************************
     * Providers
     *****************************************************************************/

    /**
     * Get mock for given trait
     * @return PHPUnit_Framework_MockObject_MockObject|Aedart\Model\Id\Interfaces\IdAware
     */
    protected function getTraitMock(){
        $m = $this->getMockForTrait('Aedart\Model\Id\Traits\UnsignedIntegerIdTrait');
        return $m;
    }

    /**
     * Get a dummy implementation of the trait
     * @return DummyUnsignedIntId
     */
    protected function getDummyImp(){
        return new DummyUnsignedIntId();
    }

    /******************************************************************************
     * Tests
     *****************************************************************************/

    /**
     * @test
     * @covers ::getDefaultId
     * @covers ::getId
     * @covers ::setId
     * @covers ::hasId
     * @covers ::isIdValid
     */
    public function getDefaultId(){
        $trait = $this->getTraitMock();
        $this->assertNull($trait->getId());
    }

//    /**
//     * NB: See next test!!!
//     *
//     * @test
//     * @covers ::getDefaultId
//     * @covers ::getId
//     * @covers ::setId
//     * @covers ::isIdValid
//     */
//    public function getIdFromCustomDefault(){
//        $trait = $this->getTraitMock();
//
//        $customDefaultId = 1234;
//
//      // !!! Only if getDefaultId was declared abstract in trait, would this work !!!
//        $trait->expects($this->any())
//            ->method('getDefaultId')
//            ->willReturn($customDefaultId);
//
//        $this->assertSame($customDefaultId, $trait->getId());
//    }

    /**
     * At the time of writing this test, PHPUnit's trait-mocking does not
     * allow you to "override" implemented methods, e.g. with the aim to change
     * their return values. Furthermore, the package "mockery/mockery": "0.9.*"
     * still has no support for traits. Therefore, to achieve high code-coverage,
     * and ensure as mush as possible works, a dummy class needed to be implemented
     * in order to test if a custom default value could be set.
     *
     * @test
     * @covers ::getDefaultId
     * @covers ::hasDefaultId
     * @covers ::getId
     * @covers ::setId
     * @covers ::hasId
     * @covers ::isIdValid
     */
    public function getIdFromCustomDefault(){
        $trait = $this->getDummyImp();

        $customDefaultId = 1234;

        $this->assertTrue($trait->hasDefaultId());
        $this->assertSame($customDefaultId, $trait->getId());
    }

    /**
     * @test
     * @covers ::getId
     * @covers ::setId
     * @covers ::hasId
     * @covers ::isIdValid
     * @covers ::hasDefaultId
     */
    public function setAndGetId(){
        $trait = $this->getTraitMock();
        $id = 42;
        $trait->setId($id);

        $this->assertFalse($trait->hasDefaultId());
        $this->assertSame($id, $trait->getId());
    }

    /**
     * @test
     * @covers ::setId
     * @covers ::isIdValid
     * @expectedException \Aedart\Model\Id\Exception\InvalidIdException
     */
    public function setInvalidIdType(){
        $trait = $this->getTraitMock();
        $id = "42";
        $trait->setId($id);
    }

    /**
     * @test
     * @covers ::setId
     * @covers ::isIdValid
     * @expectedException \Aedart\Model\Id\Exception\InvalidIdException
     */
    public function setInvalidIdRange(){
        $trait = $this->getTraitMock();
        $id = -1;
        $trait->setId($id);
    }

    /**
     * @test
     * @covers ::setId
     * @covers ::isIdValid
     * @expectedException \Aedart\Model\Id\Exception\InvalidIdException
     */
    public function setInvalidFloatId(){
        $trait = $this->getTraitMock();
        $id = 22.45;
        $trait->setId($id);
    }
}

/**
 * Class DummyUnsignedIntId
 */
class DummyUnsignedIntId {

    use UnsignedIntegerIdTrait;

    public function getDefaultId(){
        return 1234;
    }

}