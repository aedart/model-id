## Model-Id ##

Getter and Setter package for some kind of model identifier (id)

This package is part of the `Aedart\Model` namespace; visit https://bitbucket.org/aedart/model to learn more about the project.

Official sub-package website (https://bitbucket.org/aedart/model-id)

## Contents ##

[TOC]

## When to use this ##

When your component(s) need to be aware of some kind of an id

## How to install ##

```
#!console

composer require aedart/model-id 1.*
```

This package uses [composer](https://getcomposer.org/). If you do not know what that is or how it works, I recommend that you read a little about, before attempting to use this package.

## Quick start ##

Provided that you have an interface, e.g. for a person, you can extend the id-aware interface;

```
#!php
<?php
use Aedart\Model\Id\Interfaces\IdAware;

interface IPerson extends IdAware {

    // ... Remaining interface implementation not shown ...
    
}

```

In your concrete implementation, you simple use one of the id-traits;
 
```
#!php
<?php
use Aedart\Model\Id\Traits\UnsignedIntegerIdTrait;

class MyPerson implements IPerson {
 
    use UnsignedIntegerIdTrait;

    // ... Remaining implementation not shown ... 
 
}
```

## License ##

[BSD-3-Clause](http://spdx.org/licenses/BSD-3-Clause), Read the LICENSE file included in this package