<?php namespace Aedart\Model\Id\Traits;

use Aedart\Model\Id\Exception\InvalidIdException;
use Aedart\Model\Id\Interfaces\IdAware;
use Aedart\Validate\Number\Integer\UnsignedIntegerValidator;

/**
 * Trait UnsignedIntegerIdTrait
 *
 * Unsigned Integer Identifier - identifiers between 0 and PHP_INT_MAX
 *
 * @see IdAware
 * @see http://php.net/manual/en/language.types.integer.php
 * @see http://en.wikipedia.org/wiki/Integer_%28computer_science%29
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 * @package Aedart\Model\Id\Traits
 */
trait UnsignedIntegerIdTrait {

    /**
     * This component's identifier
     *
     * @var integer
     */
    protected $id = null;

    /**
     * Set this component's identifier
     *
     * @see IdAware::isIdValid($identifier)
     *
     * @param integer $identifier Some kind of identifier to be used by this component
     * @return void
     * @throws InvalidIdException If provided identifier is invalid
     */
    public function setId($identifier){
        if(!$this->isIdValid($identifier)){
            throw new InvalidIdException(sprintf('Provided id "%s" is invalid', $identifier));
        }

        $this->id = $identifier;
    }

    /**
     * Get this component's identifier
     *
     * If no identifier has been provided, this method sets and returns
     * a default, if any is available
     *
     * @see IdAware::getDefaultId()
     * @see IdAware::setId($identifier)
     *
     * @return integer|null This components identifier or <b>null</b> if there isn't any set nor any default available
     * @throws InvalidIdException If a default identifier is applied, but not valid
     */
    public function getId(){
        if(!$this->hasId() && $this->hasDefaultId()){
            $this->setId($this->getDefaultId());
        }

        return $this->id;
    }

    /**
     * Get a default identifier, if any is available
     *
     * @return integer|null A default identifier or <b>null</b> there is no default available for this component
     */
    public function getDefaultId(){
        return null;
    }

    /**
     * Check if this given component has an id defined
     *
     * @return bool True if this component has an id defined, false if not
     */
    public function hasId(){
        if(!is_null($this->id)){
            return true;
        }
        return false;
    }

    /**
     * Check if there is a default id that can be used, if  no
     * id has been provided
     *
     * @return bool True if there is a default id, false if not
     */
    public function hasDefaultId(){
        if(!is_null($this->getDefaultId())){
            return true;
        }
        return false;
    }

    /**
     * Check if the given identifier is valid
     *
     * <b>Note:</b> integer's range is checked between 0 and PHP_INT_MAX, using FILTER_VALIDATE_INT filter
     *
     * @see filter_var
     *
     * @param mixed $identifier Some kind of identifier to be validated
     * @return boolean <b>True</b> if the given identifier is valid, <b>false</b> if not
     */
    public function isIdValid($identifier){
        return UnsignedIntegerValidator::isValid($identifier);
    }

} 