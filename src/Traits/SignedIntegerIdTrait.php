<?php namespace Aedart\Model\Id\Traits;

use Aedart\Model\Id\Traits\UnsignedIntegerIdTrait;
use Aedart\Validate\Number\Integer\SignedIntegerValidator;

/**
 * Trait SignedIntegerIdTrait
 *
 * Signed Integer Identifier - identifiers between -PHP_INT_MAX and PHP_INT_MAX
 *
 * @see IdAware
 * @see http://php.net/manual/en/language.types.integer.php
 * @see http://en.wikipedia.org/wiki/Integer_%28computer_science%29
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 * @package Aedart\Model\Id\Traits
 */
trait SignedIntegerIdTrait {

    use UnsignedIntegerIdTrait;

    /**
     * Check if the given identifier is valid
     *
     * <b>Note:</b> integer's range is checked between -PHP_INT_MAX and PHP_INT_MAX, using FILTER_VALIDATE_INT filter
     *
     * @see filter_var
     *
     * @param mixed $identifier Some kind of identifier to be validated
     *
     * @return boolean <b>True</b> if the given identifier is valid, <b>false</b> if not
     */
    public function isIdValid($identifier){
        return SignedIntegerValidator::isValid($identifier);
    }

} 