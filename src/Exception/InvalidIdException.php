<?php namespace Aedart\Model\Id\Exception;

/**
 * InvalidIdException
 *
 * Throw this exception when an invalid Model Id has been provided
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 * @package Aedart\Model\Id\Exception
 */
class InvalidIdException extends \InvalidArgumentException{

} 