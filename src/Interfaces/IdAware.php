<?php namespace Aedart\Model\Id\Interfaces;

use Aedart\Model\Id\Exception\InvalidIdException;

/**
 * Interface IdAware
 *
 * "[...] identifiers (IDs) are lexical tokens that name entities [...]
 * Identifying entities makes it possible to refer to them, which is essential
 * for any kind of symbolic processing"
 * (Source Wiki, 2014, http://en.wikipedia.org/wiki/Identifier#In_computer_science)

 * Components, classes or objects that implements this interface, promise that a given value can be set, get and validated.
 * Furthermore, depending upon implementation, a default value might be returned, if no value has been set prior to obtaining it.
 *
 * @see http://en.wikipedia.org/wiki/Identifier#In_computer_science
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 * @package Aedart\Model\Id\Interfaces
 */
interface IdAware {

    /**
     * Set this component's identifier
     *
     * @see IdAware::isIdValid($identifier)
     *
     * @param mixed $identifier Some kind of identifier to be used by this component
     * @return void
     * @throws InvalidIdException If provided identifier is invalid
     */
    public function setId($identifier);

    /**
     * Get this component's identifier
     *
     * If no identifier has been provided, this method sets and returns
     * a default, if any is available
     *
     * @see IdAware::getDefaultId()
     * @see IdAware::setId($identifier)
     *
     * @return mixed|null This components identifier or <b>null</b> if there isn't any set nor any default available
     * @throws InvalidIdException If a default identifier is applied, but not valid
     */
    public function getId();

    /**
     * Get a default identifier, if any is available
     *
     * @return mixed|null A default identifier or <b>null</b> there is no default available for this component
     */
    public function getDefaultId();

    /**
     * Check if this given component has an id defined
     *
     * @return bool True if this component has an id defined, false if not
     */
    public function hasId();

    /**
     * Check if there is a default id that can be used, if  no
     * id has been provided
     *
     * @return bool True if there is a default id, false if not
     */
    public function hasDefaultId();

    /**
     * Check if the given identifier is valid
     *
     * @param mixed $identifier Some kind of identifier to be validated
     * @return boolean <b>True</b> if the given identifier is valid, <b>false</b> if not
     */
    public function isIdValid($identifier);

}